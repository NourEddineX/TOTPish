trap "exit 1" 2 9 15
[ -z "$SECRET_SEED" ] && exit 0
NOW=$(TZ="UTC" date +%s)
CODES=()
for o in $(seq $(( $NOW - 5 )) $(( $NOW + 30 )) ) ; do
	CODES+=( "$( echo -n "$o:$SECRET_SEED" | sha512sum | head -c 10 )" )
done
for e in $(seq 3); do
	if [[ ! $STATUS ]] ; then
		read -t 45 -e -p 'Enter TOTP Code: ' code || exit 1
		for i in "${CODES[@]}"; do
			code="$( echo $code | tr -d ' ' )"
			if [[ "$code" == "$i" ]] ; then
				STATUS=true
				break
			else
				STATUS=false
			fi
		done
		if [[ $STATUS ]] ; then
			break
		else
			echo -e "Wrong Code! \n"
		fi
	else
		break
	fi
done
echo $STATUS 
$STATUS || exit 1
