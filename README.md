# TOTPish
TOTP server and client in Bash  
#### Dependencies:
- bash
- coreutils

### Disclaimer : this script doesn't follow RFC6238 and it's not security-audited, use at your own risk 

## Place the script somewhere

for example place it in /usr/local/bin/

then add execute permission  to the script


```bash
chmod +x /usr/local/bin/totpsrv.sh

```

Execute the script in `.bash_profile` or `.bashrc` 

example: add the following code to `.bash_profile` :

```bash

SECRET_FILE=$HOME/.totp
export SECRET_SEED=$( cat $SECRET_FILE )
/usr/local/bin/totpsrv.sh || logout
unset SECRET_FILE
unset SECRET_SEED

```

add the secret seed to the path of `SECRET_FILE` in `.bash_profile`

```bash

echo TOP-SECRET-PASSWORD > ~/.totp

```
___


Using the client

```bash

SECRET_SEED="TOP-SECRET-PASSWORD" bash totpclnt.sh

```
