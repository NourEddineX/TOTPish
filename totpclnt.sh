if [ -z "$SECRET_SEED" ]; then
	read -e -p "TOTP Seed: " SECRET_SEED
fi
NOW=$(TZ="UTC" date +%s)
echo -n "$NOW:$SECRET_SEED"  | sha512sum | head -c 10 ; echo
